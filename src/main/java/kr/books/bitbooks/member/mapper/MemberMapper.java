package kr.books.bitbooks.member.mapper;

import kr.books.bitbooks.member.vo.MemberVO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MemberMapper {

    int registerMember(MemberVO requestMember) throws Exception;
}
