package kr.books.bitbooks.member.service;


import kr.books.bitbooks.member.mapper.MemberMapper;
import kr.books.bitbooks.member.vo.MemberVO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MemberService {

    private final MemberMapper mapper;

    public int registerMember(MemberVO requestMember) throws Exception {
        return mapper.registerMember(requestMember);
    }
}
